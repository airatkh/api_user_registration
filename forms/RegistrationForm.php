<?php
/**
 * Form for registation new User
 *
 * @author Khisamov Airat <kh.airat14@gmail.com>
 *
 */

namespace app\modules\jtapp\forms;

use Yii;
use yii\base\Model;

class RegistrationForm extends Model
{
    public $email;
    public $password_repeat;
    public $password;

    /**
     * Validation Rules
     *
     * Documentation: http://www.yiiframework.com/doc-2.0/guide-input-validation.html
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['email','password', 'password_repeat'], 'filter', 'filter' => 'trim'],
            [['email','password', 'password_repeat']   , 'required'],
            ['email', 'email'],

            ['email', 'unique',
                'targetClass' => 'app\modules\jtapp\models\User',
                'targetAttribute' => 'login',
                'message' => 'This email address has already been taken.'],

            ['password_repeat', 'compare', 'compareAttribute' => 'password'],
            ['password', 'string', 'min' => 6],
            ['password_repeat','safe']
        ];
    }
}