<?php
/**
 * Form for activation User by Code
 *
 * @author Khisamov Airat <kh.airat14@gmail.com>
 *
 * Date: 29.07.15
 * Time: 14:00
 */

namespace app\modules\jtapp\forms;

use Yii;
use yii\base\Model;
use app\modules\jtapp\models\User;


class ActivateForm extends Model
{
    public $email;
    public $code;


    /**
     * Validation Rules
     *
     * Documentation: http://www.yiiframework.com/doc-2.0/guide-input-validation.html
     *
     * @return array
     */
    public function rules()
    {
        return [

            [ ['email', 'code'],
                'filter', 'filter' => 'trim',
                'on' => ['required_validate']
            ],

            [['email', 'code'],
                'required',
                'on' => ['required_validate']
            ],

            ['email',
                'email',
                'on' => ['required_validate']
            ],

            ['email',
                'exist',
                'targetClass' => 'app\modules\jtapp\models\User',
                'targetAttribute' => 'login',
                'on' => ['required_validate'],
                'message' => 'User with this email does not exist. Please register first.'
            ],

            //Custom validation: check User activate or Not. ONLY for scenario = check_user_activated
            ['email',
                'checkUserActivated',
                'on' => ['check_user_activated']
            ],

            //Custom validation: activation code exist in cash. an inline validator defined as the model method existInCash()
            //ONLY for scenario = check_code_exist
            ['code',
                'existInCash',
                'on' => ['check_code_exist']
            ],
        ];
    }

    public function scenarios(){
        $scenarios = parent::scenarios();
        $scenarios['required_validate'] = [
            'email',
            'code',
        ];

        $scenarios['check_user_activated'] = [
            'email',
        ];

        $scenarios['check_code_exist'] = [
            'code',
        ];

        return $scenarios;
    }

    /**
     *
     * Custom validation. Check exist activation code in cache.
     * an inline validator defined as the model method existInCash()
     *
     * @param $attribute
     * @param $params
     *
     * @throws \Exception
     */
    public function existInCash($attribute, $params){

        try{
            $data = \Yii::$app->cache->get($this->$attribute);
            if($this->email != $data){
                $this->addError($attribute, 'Activation code expired or incorrect.');
            }
        }catch (\Exception $e){
            $msg = 'Cannot get from stored active code. System error: '.$e->getMessage();
            \Yii::error($msg,__METHOD__);
            $this->addError($attribute, $msg);
        }
    }

    /**
     *
     * Custom validation. Check User Activated or Not. ONLY for scenario = check_user_activated
     *
     * @param $attribute
     * @param $params
     *
     * @throws \Exception
     */
    public function checkUserActivated($attribute, $params){

        try{
            if(User::isUserActivated($this->$attribute)){
                $this->addError($attribute, 'User have been activated. Do not have to be activated again.');
            }
        }catch (\Exception $e){
            $msg = 'Cannot check User activation status. System error: '.$e->getMessage();
            \Yii::error($msg,__METHOD__);
            $this->addError($attribute, $msg);
        }
    }
}