<?php

namespace app\modules\jtapp\controllers;

use Yii;
use app\modules\core\controllers\CoreController;
use app\modules\jtapp\forms\RegistrationForm;
use app\modules\jtapp\forms\ActivateForm;
use yii\helpers\Url;
use yii\web\ServerErrorHttpException;
use app\modules\jtapp\models\User;
use yii\helpers\VarDumper;

class UserController extends CoreController
{
    protected $authExcept = false;

    public $modelClass = 'app\modules\jtapp\models\User';

    /**
     * @var string the name of the view action. This property is need to create the URL when the model is successfully created.
     */
    public $viewAction = 'view';

    /**
     * REST API registration new User. /user/registration
     *
     * Validation Form
     *
     * POST data = {
     *      email,
     *      password,
     *      password_repeat
     *     }
     *
     * @return RegistrationForm app\modules\jtapp\forms\RegistrationForm
     * @throws ServerErrorHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionRegistration()
    {
        $form = new RegistrationForm();
        $form->load(\Yii::$app->getRequest()->getBodyParams(), '');
        if ($form->validate()) {
            $user = $this->registration($form);
            $msg = 'User have been created .System information: given RegistrationForm = :'.VarDumper::export($form->getAttributes());
            $msg = $msg.' User = '.VarDumper::export($user->getAttributes());
            \Yii::info($msg,__METHOD__);
            return $user;
        } elseif (!$form->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create User object for unknown reason.');
        }
        return $form;
    }

    /**
     * REST API: activate User account by code from e-mail
     *
     * <code>
     *      GET /user/activate?email=hairat@nnm.com&code=03923408-6251-4b09-9386-8547ceb60010
     * </code>
     *
     * @return ActivateForm|User app\modules\jtapp\forms\ActivateForm
     *
     * @throws ServerErrorHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionActivate()
    {
        //Используеться поэтапная валидация. Т.к. одна валидация полей зависит от валидации этих поле по другим признакам.
        $form = new ActivateForm(['scenario' => 'required_validate']);
        $form->load(\Yii::$app->getRequest()->get(), '');
        if ($form->validate()) {
            $form->scenario = 'check_user_activated';
            if ($form->validate()) {
                $form->scenario = 'check_code_exist';
                if ($form->validate()) {
                    $user = User::activateUser($form);
                    $msg = 'User activated by code.System information: given activate_form = :'.VarDumper::export($form->getAttributes());
                    $msg = $msg.' User = '.VarDumper::export($user->getAttributes());
                    \Yii::info($msg,__METHOD__);
                    $token = \app\modules\core\models\AccessToken::generateForUser($user);
                    return [
                        'user' => $user,
                        'token' => $token
                    ];
                }
            }
        } elseif (!$form->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }
        return $form;
    }

    /**
     * Create new User
     *
     * @param $form \app\modules\jtapp\forms\RegistrationForm;
     *
     * @return User
     * @throws \Exception
     */
    public function registration($form)
    {
        if( !($form instanceof RegistrationForm) ){
            $msg = 'Error instance type: param activate_form have to be instanceof ActivateForm. System information: given activate_form = :'.VarDumper::export($form);
            \Yii::error($msg,__METHOD__);
            throw new \Exception($msg);
        }
        $user = User::newUserCreate($form->email, $form->password);
        if(!$user->hasErrors()){
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $id = implode(',', array_values($user->getPrimaryKey(true)));
            $response->getHeaders()->set('Location', Url::toRoute([$this->viewAction, 'id' => $id], true));
        }
        return $user;
    }
}
