<?php
/**
 *
 * Данный скрипт выполняет отправку писем с кодом активизации аккаунта.
 *
 * Date: 06/08/2015
 *
 * @author Airat Khisamov <kh.airat14@gmail.com>
 */

namespace app\commands;

use Yii;

use app\modules\jtapp\models\User;
use yii\console\Controller;
use yii\helpers\VarDumper;
use yii\helpers\Url;


/**
 * This command send email with user's activation link.
 *
 * @author Khisamov Airat <kh.airat14@gmail.com>
 */
class SendEmailWithActivateLinkController extends Controller
{

    /**
     * Алгоритм работы worker перекодировки(альфа версия):
     * 1. Получить задачу на отправку email
     * <code>
     *        $data = [
     *           'cmd'=>'activate_code',
     *           'login'=>login,
     *           'active_code'=>$active_code
     *           ];
     * <code>
     * 1 Проверка структуры данных , пришедших данных
     * 2.Проверка окружения значение params
     * 2 Сформировать link для активации.
     * 3.Сформировать текс письма
     * 4. Отправить письмо на почтовый ящик
     */

    /**
     * @var array Data enter for worker from RabbitMg
     */
    private $data;


    /**
     * 1 Проверка структуры данных , пришедших данных
     *
     * @param array $data
     *
     * @throws \Exception
     */
    protected function check_entry_data($data=[]){

        $keys = ['cmd', 'login', 'active_code'];
        foreach($keys as $key){
            if( (!isset($data[$key]) || empty($data[$key])) ){
                $msg = 'Incorrect params. Can not find '.$key.' key. Data array recieved == '. VarDumper::export($data);
                \Yii::error($msg,__METHOD__);
                throw new \Exception($msg);
            }
        }
        $this->data = $data;
    }

    /**
     * Получение параметра из конфигурационного файла
     *
     * @param string $param_name название пареметра
     *
     * @return string Значение параметра
     * @throws \Exception
     */
    protected function getConfigParam($param_name){

        if ( isset(Yii::$app->params[$param_name]) ) {
            return Yii::$app->params[$param_name];
        }else {
            $msg = "Не удалось найти параметр в конфигурационном файле. Искали" . $param_name;
            \Yii::error($msg,__METHOD__);
            throw new \Exception($msg);
        }

    }

    /**
     * Send e-mail to user.
     *
     * @throws \Exception
     */
    protected function send_email(){
        //$user = $this->loadUser(); //TODO Надо понять, нужен User  письме.
        $adminEmail = $this->getConfigParam('adminEmail');
        $url = $this->getActivateLink();

        try {
            $mailer = Yii::$app->get('mailer');
            $message = $mailer->compose('@app/mail/activation_email_rus', ['url' => $url])
                ->setFrom($adminEmail)
                ->setTo($this->data['login'])
                ->setSubject('WayJournal.com account activation');

            //Debug information for action "mail send"
            $logger = new \Swift_Plugins_Loggers_ArrayLogger();
            $mailer->getSwiftMailer()->registerPlugin(new \Swift_Plugins_LoggerPlugin($logger));
            if (!$message->send()) {
                $msg = "Error: Cannot send email. System error = " . VarDumper::export($logger->dump());
                \Yii::error($msg, __METHOD__);
                throw new \Exception($msg);
            }
        } catch(\Exception $e) {
            $msg = 'Error happen while sending email. System error: '.$e->getMessage();
            \Yii::error($msg,__METHOD__);
            throw new \Exception($msg);
        }
    }

    /**
     * Search User by login
     *
     * @return null|static
     * @throws \Exception
     */
    protected function loadUser(){
        $user = User::findOne(['login'=>$this->data['login']]);
        if($user == null){
            $msg = "Error: Can not find user with login = ". $this->data['login'] ." Received data = ".VarDumper::export($this->data);
            \Yii::error($msg,__METHOD__);
            throw new \Exception($msg);
        }
        return $user;
    }

    /**
     * Create Activation Account Link
     * Examples:
     * <code>
     *    http://airat.local/user/activate?email=hairat@nnm.com&code=03923408-6251-4b09-9386-8547ceb60010
     * </code>
     *
     * Examples: //TODO delete after test AiratKH(07.08.2015)
     * http://buddyschool.com/user/activation/9d69eea680189bd5ed03195fecf59d153474215d/kh.airat14@gmail.com
     * http://www.draftsight.com/webservices/activation.htm?id=6281411&key=U7P2OCNW1J
     */
    protected function getActivateLink(){
        try {
            $url = Url::toRoute([ 'user/activate', 'email' => $this->data['login'], 'code' => $this->data['active_code'] ]);
            return $url;
        } catch(\Exception $e) {
            $msg = 'Error happen while create Activate Link. System error: '.$e->getMessage();
            \Yii::error($msg,__METHOD__);
            throw new \Exception($msg);
        }
    }


    /**
     * This main action
     * start send e-mail with activation code.
     *
     * Example data:
     * <code>
     *         $data =[
     *           'cmd'=>'activate_code',
     *           'login'=>'user_name@hostname.ru',
     *           'active_code'=>'some_active_code_here'
     *           ]
     * </code>
     *
     * @return bool
     * @internal param string $message the message to be echoed.
     */
    public function actionIndex($data = [])
    {
        $msg = "Send Email with Activate Link task started. The obtained data ==".VarDumper::export($data);
        \Yii::info($msg,__METHOD__);
        $this->check_entry_data($data);
        $this->send_email();
        return true;
    }
}
