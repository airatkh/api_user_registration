<div>
    <p>
        <span style="font: bold 16px Arial, sans-serif; line-height: 24px; color: #000000;">Поздравляю!</span>
    </p>
    <p>
        <span style="font: bold 16px Arial, sans-serif; line-height: 24px; color: #000000;">
            Вы успешно создали свой аккаунт WayJournal.
        </span>
    </p>
    <p>
        <span style="font: bold 16px Arial, sans-serif; line-height: 24px; color: #000000;">
            Для активации Вашей учетной записи, пожалуйста, подтвердите правильность Вашего адреса электронной почты.
        </span>
    </p>

    <p>
        <span style="font: bold 16px Arial, sans-serif; line-height: 24px; color: #000000;">
            <a href="<?php echo $url ?>">Нажмите кнопку, чтобы активировать адрес электронной почты &raquo;</a>
        </span>
    </p>
</div>
