# Регистрация#
[[Registration]] — Регистация нового пользователя и получения письма с кодом активации пользователя. 
Активицация пользователя и получение [Access Token](https://redmine.wayjournal.com/projects/boxy/wiki/Headers#Access-Token) для дальнейшей работы


#Registration#

Регистрация пользователя в системе происходит в два этапа.

* Регистрация пользователя с отправкой e-mail (login) на почтовый ящик c ссылкой для активизазии аккаунта.

* Активицация пользователя и получение [Access Token](https://redmine.wayjournal.com/projects/boxy/wiki/Headers#Access-Token) для дальнейшей работы


## [1. Регистрация пользователя](https://bitbucket.org/airatkh/api_user_registration/src/79e58ac9ae23d9d473817192b55a3af63ca73138/controllers/UserController.php?at=master)##

```
#!api
POST user/registration
```



*POST data*
```
#!json
{
    "email": "user_email@somehostname.com",
    "password": "some_password",
    "password_repeat": "some_password",
}
```


**Пользователь создан. На e-mail отправлено письмо с кодом активизации.**

STATUS 204
```
#!json
{
    "uid": "52fac8f1-b9a7-51a6-be65-6084669302ef",
    "login": "user_email@somehostname.com",
}
```


###Типы сообщений об ошибки валидации###


STATUS 422
```
#!json
[
  {
    "field": "email",
    "message": "Email cannot be blank."
  },
  {
    "field": "password",
    "message": "Password cannot be blank."
  },
  {
    "field": "password_repeat",
    "message": "Password_repeat cannot be blank."
  }
]
```


STATUS 422
```
#!json
[
  {
    "field": "email",
    "message": "Email is not a valid email address."
  }
]
```

STATUS 422
```
#!json
[
  {
    "field": "email",
    "message": "This email address has already been taken"
  }
]
```

STATUS 422
```
#!json
[
  {
    "field": "password_repeat",
    "message": "Password Repeat must be repeated exactly."
  }
]
```

## [2. Активация пользователя](https://bitbucket.org/airatkh/api_user_registration/src/79e58ac9ae23d9d473817192b55a3af63ca73138/controllers/UserController.php?at=master)##
```
#!api
GET user/activate?email=hairat@nnm.com&code=some_code_here
```

*GET data*
```
#!json
{
    "email": "user_email@somehostname.com",
    "code": "code_from_email",
}
```

### Пользователь активирован###


STATUS 200
```
#!json
{
  "user": {
    "uid": "b3d7311d-c15c-49f9-a11b-80be14c105dd",
    "login": "hairat@nnm.com"
  },
  "token": {
    "id": "ab9c32fcbc3ebe5eb757d723868dcfe3",
    "user_uid": "b3d7311d-c15c-49f9-a11b-80be14c105dd",
    "r": 1,
    "w": 1,
    "ip": "192.168.78.93",
    "la": 1438942009,
    "d": {
      "device": "",
      "login": "hairat@nnm.com"
    },
    "ttl": 604800
  }
}
```


### Типы сообщений об ошибки валидации###


STATUS 422
```
#!json
[
  {
    "field": "email",
    "message": "Email cannot be blank."
  },
  {
    "field": "code",
    "message": "Code cannot be blank."
  }
]
```

STATUS 422
```
#!json
[
  {
    "field": "email",
    "message": "Email is not a valid email address."
  }
]
```


STATUS 422
```
#!json
[
  {
    "field": "email",
    "message": "User have been activated. Do not have to be activated again."
  }
]
```


STATUS 422
```
#!json
[
  {
    "field": "email",
    "message": "User with this email does not exist. Please register first."
  }
]
```

STATUS 422
```
#!json
[
  {
    "field": "code",
    "message": "Activation code expired or incorrect."
  }
]
```



# Workflow user create /activate #

# 1. User create#

### [1.1 Configuration](https://bitbucket.org/airatkh/api_user_registration/src/ee083fd88c108a2a5ed2c76093b68ee6274b9a56/config/web.php?at=master) ###

```
#!api
POST user/registration' => jtapp/user/registration
```


### [1.2  Action](https://bitbucket.org/airatkh/api_user_registration/src/ee083fd88c108a2a5ed2c76093b68ee6274b9a56/controllers/UserController.php?at=master)###

```
#!api
controller/UserController actionRegistration()
```


### [1.3 Validation form](https://bitbucket.org/airatkh/api_user_registration/src/ee083fd88c108a2a5ed2c76093b68ee6274b9a56/forms/RegistrationForm.php?at=master)###

```
#!api
forms/RegistrationForm rules()
```

### [1.4 Main method in model](https://bitbucket.org/airatkh/api_user_registration/src/ee083fd88c108a2a5ed2c76093b68ee6274b9a56/models/User.php?at=master)###

```
#!api
models/User newUserCreate() 
```


### 1.5 Create task send email with activation link ###

```
#!api
models/User putTaskSendEmailWithLink2ActiveCode() 
```

# [2.  Send e-mail with activation link](https://bitbucket.org/airatkh/api_user_registration/src/ee083fd88c108a2a5ed2c76093b68ee6274b9a56/commands/SendEmailWithActivateLinkController.php?at=master) #

```
#!api
commands/SendEmailWithActivateLinkController actionIndex()
```


# 3. User activate#

### [3.1 Configuration](https://bitbucket.org/airatkh/api_user_registration/src/ee083fd88c108a2a5ed2c76093b68ee6274b9a56/config/web.php?at=master) ###

```
#!api
config/web.php 
'GET user/activate' => 'jtapp/user/activate',
```


### [3.2  Action](https://bitbucket.org/airatkh/api_user_registration/src/ee083fd88c108a2a5ed2c76093b68ee6274b9a56/controllers/UserController.php?at=master)###

```
#!api
controller/UserController actionActivate()
```

### [3.3 Validation form](https://bitbucket.org/airatkh/api_user_registration/src/ee083fd88c108a2a5ed2c76093b68ee6274b9a56/forms/ActivateForm.php?at=master)###

```
#!api
forms/ActivateForm rules() 
```
###[3.4 Main method in model](https://bitbucket.org/airatkh/api_user_registration/src/ee083fd88c108a2a5ed2c76093b68ee6274b9a56/models/User.php?at=master)###

```
#!api
models/User activateUser() 
```