<?php
/**
 * Created by PhpStorm.
 * User: EagleMoor
 * Date: 23.01.15
 * Time: 15:28
 */

namespace app\modules\jtapp\models;


use yii\helpers\VarDumper;
use yii\db\Expression;
use app\modules\jtapp\forms\ActivateForm;
use yii\web\ServerErrorHttpException;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property string $uuid
 * @property string $login
 * @property string $password
 * @property integer $org_type
 * @property integer $is_active
 * @property string $parent_uid
 * @property string $photo_uid
 * @property string $user_type_uid
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property string $created_by
 * @property string $updated_by
 *
 * @property Car[] $cars
 */
class User extends \app\modules\core\models\User
{
    public static $accessTokenClass = 'app\modules\core\models\WJAccessToken';
    public $carClass = 'app\modules\jtapp\models\Car';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    public function fields()
    {
        return [
            'uid' => 'uuid',
            'login',
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uuid'], 'required'],
            [['org_type', 'is_active'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['uuid', 'parent_uid', 'photo_uid', 'user_type_uid', 'created_by', 'updated_by'], 'string', 'max' => 36],
            [['login'], 'string', 'max' => 50],
            [['password'], 'string', 'max' => 40],
            [['login'], 'unique']
        ];
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return \Yii::$app->get('db_wj');
    }

    /**
     * Валидация пароля пользователя.
     *
     * @param $password
     *
     * @return bool
     */
    public function validatePassword($password)
    {
        $passwordParams = \Yii::$app->params['password'];
        return $this->password === $passwordParams['method']($password . $passwordParams['salt']);
    }

    /**
     * REST API new user create.
     *
     * POST  user/registration/
     * <code>
     *  [
     *   ‘email’ = some_name@mail_+domain.com
     *   ‘password’ = super_password
     *   ]
     * </code>
     *
     * Create inactive user model, next step create default car model.
     *
     * @param $login    string
     * @param $password string
     *
     * @return User
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public static function newUserCreate($login, $password){

        $transaction = User::getDb()->beginTransaction();
        try {
            $user = new self();
            $user->login = $login;
            $params = \Yii::$app->params['password'];
            $user->password = $params['method']($password . $params['salt']);
            //TODO generate uuid
            $user->uuid = self::generateActiveUserCode();
            $user->created_at = new Expression('NOW()');
            $user->updated_at = new Expression('NOW()');
            $user->updated_by = $user->uuid;
            $user->created_by = $user->uuid;
            $user->is_active = 0;

            if (!$user->save()) {
                $msg = 'Error while create user. System error: '.VarDumper::export($user->getErrors());
                \Yii::error($msg,__METHOD__);
                throw new \Exception($msg);
            }
            $user->onCreateUser();
            $transaction->commit();
            return $user;
        } catch(\Exception $e) {
            $transaction->rollBack();
            $user->addError('login', $e->getMessage() );
            $msg = 'Cannot create new User. User = . System error: '.$e->getMessage();
            \Yii::error($msg,__METHOD__);
            return $user;
        }
    }

    /**
     * Trigger for while create new User
     *
     * @throws \Exception
     */
    public function onCreateUser(){
        $this->defaultCarCreate();
        $active_code = self::generateActiveUserCode();
        $this->setActiveCode2Cache($active_code);
        $this->putTaskSendEmailWithLink2ActiveCode($active_code);
    }

    /**
     * Данный метод выполняет действия после регистрации пользователя в системе.
     * Реализовано создание автомобиля по умолчанию.
     *
     * @throws \Exception
     * @internal param string $user_uid uuid пользователя который был создан.
     *
     */
    public function defaultCarCreate() {
        try {
            $car = new Car();
            $car->user_uid = $this->uuid;
            $car->save(false); // без валидации, тк vendor & model пустые
        }catch (\Exception $e){
            $msg = 'Cannot create new default Car for account. System error: '.$e->getMessage();
            \Yii::error($msg,__METHOD__);
            throw new \Exception($msg);
        }
    }

    /**
     * Generate code for activate( make validation) email address
     * Generate uuid
     * For more information:
     * https://en.wikipedia.org/wiki/Universally_unique_identifier
     *
     * @return string
     */
    public static function generateActiveUserCode(){
        if (function_exists('com_create_guid') === true) {
            return strtolower(trim(com_create_guid(), '{}'));
        }
        $uuid = sprintf(
            '%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535)
        );
        $uuid = strtolower($uuid);
        return $uuid;
    }

    /**
     * Save active code to cache
     *
     * @internal param string $key
     * @internal param string $email
     */
    public function setActiveCode2Cache($active_code){
        //259,200 трое суток
        try{
            if(\Yii::$app->cache->set($active_code, $this->login, 259200) != true){
                $msg = 'Cannot store active code to cache.';
                \Yii::error('Cannot store active code to cache.',__METHOD__);
                throw new \Exception($msg);
            }
        }catch (\Exception $e){
            $msg = 'Cannot store(set) active code to cache. System error: '.$e->getMessage();
            \Yii::error($msg,__METHOD__);
            throw new \Exception($msg);
        }
    }

    /**
     * Get user login by activation key from cache
     * Возвращает login(e-mail) пользователя из кеша ассоциированный с кодом активации.
     *
     * @param $active_code
     *
     * @return string|null    Возвращает login(e-mail) пользователя из кеша ассоциированный с кодом активации.
     *                        Или null если не найдена запись в кеше или срок действий ключа истек.
     * @throws \Exception
     */
    public static function getActiveCodeFromCache($active_code){
        try{
            $data = \Yii::$app->cache->get($active_code);
            return $data==false ? null : $data;
        }catch (\Exception $e){
            $msg = 'Cannot get from store active code. System error: '.$e->getMessage();
            \Yii::error($msg,__METHOD__);
            throw new \Exception($msg);
        }
    }

    /**
     * The main activation process for user
     *
     * @param $activate_form \app\modules\jtapp\forms\ActivateForm
     * @return bool
     * @throws ServerErrorHttpException
     * @throws \Exception
     */
    static public function activateUser($activate_form){

        if( !($activate_form instanceof ActivateForm) ){
            $msg = "Error instance type: param activate_form have to be instanceof ActivateForm. ";
            $msg = $msg." System information: given activate_form = :".VarDumper::export($activate_form->getAttributes());
            \Yii::error($msg,__METHOD__);
            throw new \Exception($msg);
        }

        if( $activate_form->email != self::getActiveCodeFromCache($activate_form->code)){
            $msg = "Activate code incorrect or expired. Please request new activation code. ";
            $msg = $msg. "System information: Given data = :".VarDumper::export($activate_form->getAttributes());
            \Yii::error($msg,__METHOD__);
            throw new \Exception($msg);
        }

        $user=self::findByLogin($activate_form->email);
        if( !($user instanceof User) ){
            $msg = 'User does not exist with this Email. System information: Given data = :'.VarDumper::export($activate_form->getAttributes());
            \Yii::error($msg,__METHOD__);
            throw new \Exception($msg);
        }

        if($user->setVerifiedEmail()){
            return $user;
        }else{
            $msg = 'Failed to activate user for unknown reason. System information: Given data = :'.VarDumper::export($activate_form->getAttributes());
            \Yii::error($msg,__METHOD__);
            throw new ServerErrorHttpException($msg);
        }
    }

    /**
     * Update user status. Set is_active to  true.
     *
     * @return bool Success updated
     * @throws \Exception Error happened while activate User.
     */
    public function setVerifiedEmail(){
        $this->is_active = 1;
        if($this->save()){
            return true;
        }else{
            $msg = 'Error happened while activate User. System error = '.VarDumper::export($this->getErrors());
            \Yii::error($msg,__METHOD__);
            throw new \Exception($msg);
        }
    }

    /**
     * Check User Activated or Not.
     *
     * @param $login
     *
     * @return bool true - if user activated or false - if user does not activated
     * @throws \Exception
     */
    public static function isUserActivated($login){

        $user=self::findByLogin($login);
        if( !($user instanceof User) ){
            $msg = 'User does not exist with this Email. System information: Given data = :'.VarDumper::export($activate_form);
            \Yii::error($msg,__METHOD__);
            throw new \Exception($msg);
        }
        return $user->is_active == 1 ? true : false;
    }

    /**
     * Send task (data) to RabbitMq.
     * Task: Send e-mail with link to activate user account
     *
     * Rabbit Queue name: 'email-activate'
     *
     * This task for worker (commands)SendEmailWithActivateLinkController
     *
     * Task structure:
     * <code>
     *         $data = [
     *           'cmd'=>'send_activation_code',
     *           'login'=>'login',
     *           'active_code'=>'active_code'
     *           ];
     * </code>
     *
     */
    public function putTaskSendEmailWithLink2ActiveCode($active_code){
        $data = [
            'cmd'=>'send_activation_code',
            'login'=>$this->login,
            'active_code'=>$active_code
        ];
        $msg = "Set task to RabbitMq. Send email with active code : ".VarDumper::export($data);
        \Yii::info($msg,__METHOD__);
        $odbrabbitmq = \Yii::$app->odbrabbitmq;
        $odbrabbitmq->send('email-activate', 'email-activate', $data);
    }
}