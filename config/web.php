<?php

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'sourceLanguage' => 'en-US',
    'language' => 'en',
    'bootstrap' => [
        'log',
        'core',
        [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => [
                'application/json' => \yii\web\Response::FORMAT_JSON,
                'text/html' => \yii\web\Response::FORMAT_HTML,
                '*/*' => \yii\web\Response::FORMAT_JSON,
                'application/xml' => \yii\web\Response::FORMAT_XML,
            ],
            'languages' => [
                'en',
                'ru',
            ],
        ]
    ],
    'components' => [
        'request' => [
            'enableCookieValidation' => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\modules\jtapp\models\User',
            'enableSession' => false,
            'loginUrl' => null
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\boxy\logs\RestTarget',
                    'categories' => ['rest'],
                    'levels' => ['info'],
                    'logFile' => '@runtime/logs/rest.log',
                    'logVars' => ['_REST', '_GET', '_POST'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/user_registration_activate.log',
                    'levels' => ['info', 'error', 'warning'],
                    'categories' => ['app\modules\jtapp\*' ],
                    'exportInterval'=>1,
                    'maxLogFiles'=>5,
                    'enabled'=>true,
                    'logVars'=>[],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                'POST user/registration' => 'jtapp/user/registration',
                'GET user/activate' => 'jtapp/user/activate',
            ],
        ],
    ],
    'params' => $params,
    'modules' => [
        'jtapp' => 'app\modules\jtapp\Module',
    ]
];

return $config;
